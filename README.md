# melon-cli

A simple CLI for quickly developing web applications using React.

### Installation

Prerequisites: [Node.js](https://nodejs.org/) (>=4.0) and [Git](https://git-scm.com/).

```bash
$ npm install -g melon-cli
```

### Usage

```bash
$ melon init <template-name> <project-name>
```
### Example

```bash
$ webpack init webpack my-project
```

### Templates

Current available templates include:

- [webpack](https://gitlab.com/wonsure2/react-template-webpack) - A Webpack + React setup with hot reload, linting, testing.

### License

[MIT](http://opensource.org/licenses/MIT)
