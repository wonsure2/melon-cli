var async = require('async');
var inquirer = require('inquirer');
var evaluate = require('./eval');

module.exports = function ask(prompts, data, done) {
    async.eachSeries(Object.keys(prompts), function (key, next) {
        prompt(data, key, prompts[key], next);
    }, done);
};

function prompt(data, key, prompt, done) {
    if (prompt.when && !evaluate(prompt.when, data)) { // metalsmith.metadata() doesn't contain prompt.when
        return done; // next one in the series
    }

    var promptDefault = prompt.default;
    if (typeof prompt.default === 'function') {
        promptDefault = function () {
            return prompt.default.bind(this)(data); // it needs more data besides answers.
        }
    }

    inquirer.prompt({
        type: prompt.type,
        name: key,
        message: prompt.message || key,
        default: promptDefault,
        choices: prompt.choices || [],
        validate: prompt.validate || function () { return true; }
    }).then(function (answers) {
        if (Array.isArray(answers[key])) {
            data[key] = {};
            answers[key].forEach(function (multiChoiceAnswer) {
                data[key][multiChoiceAnswer] = true;
            });
        } else {
            data[key] = answers[key];
        }
        done();
    });
}