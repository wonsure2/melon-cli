var Metalsmith = require('metalsmith');
var Handlebars = require('handlebars');
var async = require('async');
var render = require('consolidate').handlebars.render;
var path = require('path');
var multimatch = require('multimatch');
var getOptions = require('./options'); // ??
var ask = require('./ask'); // ??
var filter = require('./filter'); // ??

Handlebars.registerHelper('if_eq', function (a, b, options) {
    return a === b ? options.fn(this) : options.inverse(this);
});

Handlebars.registerHelper('unless_eq', function (a, b, options) {
    return a === b ? options.inverse(this) : options.fn(this);
});

module.exports = function generate(name, src, dest, done) {
    var opts = getOptions(name, src);
    opts.helpers && Object.keys(opts.helpers).forEach(function (key) {
        Handlebars.registerHelper(key, opts.helpers[key]);
    });

    var metalsmith = Metalsmith(path.join(src, 'template'));
    var data = Object.assign(metalsmith.metadata(), { // add configs to metalsmith's global metadata.
        destDirName: name,
        inPlace: dest === process.cwd(),
        noEscape: true
    });
    metalsmith
        .use(askQuestions(opts.prompts))
        .use(filterFiles(opts.filters)) // filter files based on answers
        .use(renderTemplateFiles(opts.skipInterpolation))
        .clean(false) // do not remove the destination directory before writing to it
        .source('.') // the default is ./src
        .destination(dest)
        .build(function (err) {
            done(err);
            logMessage(opts.completeMessage, data);
        });
    return data;
};

/**
 * middlewares
 */

function askQuestions(prompts) {
    return function (files, metalsmith, done) {
        ask(prompts, metalsmith.metadata(), done); // done triggers the next step.
    };
}

function filterFiles(filters) {
    return function (files, metalsmith, done) {
        filter(files, filters, metalsmith.metadata(), done);
    }
}

function renderTemplateFiles(skipInterpolation) {
    skipInterpolation = typeof skipInterpolation === 'string'
        ? [skipInterpolation]
        : skipInterpolation;
    return function (files, metalsmith, done) {
        var fileKeys = Object.keys(files);
        var metalsmithMetadata = metalsmith.metadata();
        async.each(fileKeys, function (file, next) {
            if (skipInterpolation && multimatch([file], skipInterpolation, { dot: true }).length) {
                return next();
            }
            var str = files[file].contents.toString();
            // skip files without {{ mustaches }}
            if (!/{{([^{}]+)}}/g.test(str)) {
                return next();
            }
            render(str, metalsmithMetadata, function (err, res) {
                if (err) {
                    return next(err);
                }
                files[file].contents = new Buffer(res);
                next();
            });
        }, done)
    };
}

/**
 * log method
 */
function logMessage(message, data) {
   if (!message) {
       return;
   }
   render(message, data, function (err, res) {
       if (err) {
           console.error('   Error when rendering template complete message: ' + err.message.trim() + '.');
       } else {
           console.log(
               res.split(/\r?\n/g).map(function (line) {
                   return '   ' + line;
               }).join('\n')
           );
       }
   });
}